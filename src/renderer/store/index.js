import Vue from 'vue';
import Vuex from 'vuex';

import { createPersistedState } from 'vuex-electron';
const fs = require('fs');

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [
    createPersistedState(),
  ],
  strict: process.env.NODE_ENV !== 'production',

  state: {
    settings: {
      user: null,
    },
    projects: [],
    special: [
      { title: 'Отгулы', time: 0, comment: '' },
      { title: 'Больничный лист', time: 0, comment: '' },
      { title: 'Отпуск', time: 0, comment: '' },
      { title: 'ОБСЗ', time: 0, comment: '' },
      { title: 'Непрофильные задачи', time: 0, comment: '' },
      { title: 'Простои', time: 0, comment: '' },
      { title: 'Дополнительно оплаченное время', time: 0, comment: '' },
    ],
  },
  actions: {
    loadData({ commit }) {
      if (!fs.existsSync('./db.json')) {
        commit('saveData');
      }
      const data = fs.readFileSync('./db.json', 'utf8');
      const { settings, projects, special } = JSON.parse(data);
      commit('setSettings', settings);
      commit('setSpecial', special);
      commit('setProjects', projects);
      console.log(JSON.stringify(projects));
    },

    addProject({ commit }, project) {
      commit('addProject', project);
      commit('saveData');
    },

    removeProject({ commit }, projectId) {
      commit('removeProject', projectId);
      commit('saveData');
    },

    saveSpecial({ commit }, special) {
      commit('setSpecial', special);
      commit('saveData');
    },
  },

  mutations: {
    saveData(state) {
      const data = JSON.stringify(state);
      fs.writeFileSync('./db.json', data);
    },

    setProjects(state, payload) {
      state.projects = payload;
    },
    setSettings(state, payload) {
      state.settings = payload;
    },
    setSpecial(state, payload) {
      state.special = payload;
    },
    addProject(state, project) {
      state.projects.push(project);
    },
    removeProject(state, projectId) {
      state.projects = state.projects.filter(project => project.id !== projectId);
    },
  },
});
