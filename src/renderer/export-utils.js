const fs = require('fs');
// eslint-disable-next-line import/no-extraneous-dependencies
const { dialog } = require('electron').remote;

function generateSecondPage(userName, special) {
  return `<Worksheet>
      <Table>
        <Row>
          <Cell><Data>empl_</Data></Cell>
          <Cell><Data>${userName}</Data></Cell>
          <Cell><Data>1</Data></Cell>
          <Cell><Data>комментарии</Data></Cell>
        </Row>
        ${special.map(item => `
          <Row>
            <Cell><Data>${item.title}</Data></Cell>
            <Cell><Data>${item.time}</Data></Cell>
            <Cell><Data>${item.comment}</Data></Cell>
          </Row>
        `)
    .join('')}
      </Table>
    </Worksheet>`;
}

function generateProjectTasks(project, userName) {
  console.log(JSON.stringify(project.tasks));
  return `
    <Row>
      <Cell><Data>prnm_</Data></Cell>
      <Cell><Data>${project.name}</Data></Cell>
    </Row>
    <Row>
      <Cell><Data>empl_</Data></Cell>
      <Cell><Data>${userName}</Data></Cell>
    </Row>
      ${project.tasks.map(task => `
        <Row>
          <Cell><Data>${task.name}</Data></Cell>
          <Cell><Data>${task.time}</Data></Cell>
        </Row>
      `).join('')}
  `;
}

function generateFirstPage(userName, items) {
  return `
    <Worksheet>
      <Table>
        ${items.map(project => generateProjectTasks(project, userName)).join('')}
      </Table>
    </Worksheet>
   `;
}

export default function exportData(tasks, settings, special) {
  let xmlBook = '<Workbook>';
  xmlBook += generateFirstPage(settings.user, tasks);
  xmlBook += generateSecondPage(settings.user, special);
  xmlBook += '</Workbook>';

  dialog.showSaveDialog({ defaultPath: 'report.xml' }, (filename) => {
    if (filename) {
      fs.writeFile(filename, xmlBook, () => {
        dialog.showMessageBox({
          type: 'info',
          message: 'Файл экпортирован',
          title: 'Экспорт',
        });
      });
    }
  });
}
