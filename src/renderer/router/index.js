import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/tasks',
      name: 'tasks',
      component: require('@/components/Tasks').default,
    },
    {
      path: '/settings',
      name: 'settings',
      component: require('@/components/Settings').default,
    },
    {
      path: '/special',
      name: 'special',
      component: require('@/components/SpecialTasks').default,
    },
    {
      path: '/projects',
      name: 'projects',
      component: require('@/components/Projects').default,
    },
    {
      path: '*',
      redirect: '/tasks',
    },
  ],
});
